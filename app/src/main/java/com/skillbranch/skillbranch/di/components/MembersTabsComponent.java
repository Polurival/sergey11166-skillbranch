package com.skillbranch.skillbranch.di.components;

import com.skillbranch.skillbranch.di.anotations.PerFragment;
import com.skillbranch.skillbranch.di.modules.MembersTabsModule;
import com.skillbranch.skillbranch.ui.presenters.MembersTabsPresenter;
import com.skillbranch.skillbranch.ui.views.fragments.MembersTabsFragment;

import dagger.Subcomponent;

/**
 * @author Sergey Vorobyev.
 */
@PerFragment
@Subcomponent(modules = MembersTabsModule.class)
public interface MembersTabsComponent {

    void inject(MembersTabsFragment fragment);

    MembersTabsPresenter presenter();
}
