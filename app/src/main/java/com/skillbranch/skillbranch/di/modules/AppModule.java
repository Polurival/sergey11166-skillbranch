package com.skillbranch.skillbranch.di.modules;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.skillbranch.skillbranch.App;
import com.skillbranch.skillbranch.data.db.entities.DaoMaster;
import com.skillbranch.skillbranch.data.db.entities.DaoSession;
import com.skillbranch.skillbranch.data.network.PicassoCache;
import com.skillbranch.skillbranch.data.network.RestService;
import com.skillbranch.skillbranch.data.network.ServiceGenerator;
import com.squareup.picasso.Picasso;

import org.greenrobot.greendao.database.Database;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev
 */
@Module
public class AppModule {

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(App.get());
    }

    @Provides
    @Singleton
    RestService provideRestService() {
        return ServiceGenerator.createService(RestService.class);
    }

    @Provides
    @Singleton
    Picasso providePicasso() {
        return new PicassoCache().getPicassoInstance();
    }

    @Provides
    @Singleton
    DaoSession provideDaoSession() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(App.get(), "skill-brunch-db");
        Database database = helper.getWritableDb();
        return new DaoMaster(database).newSession();
    }
}
