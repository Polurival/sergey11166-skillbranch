package com.skillbranch.skillbranch.ui.views;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.skillbranch.skillbranch.data.db.entities.MemberEntity;

/**
 * @author Sergey Vorobyev.
 */

public interface MemberDetailsView extends MvpView {

    void showData(MemberEntity data, MemberEntity father, MemberEntity mother);
    void navigateToAnotherMember(long id);
    void showDied(String data);
}
