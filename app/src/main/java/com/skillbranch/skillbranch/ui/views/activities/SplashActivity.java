package com.skillbranch.skillbranch.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.skillbranch.skillbranch.App;
import com.skillbranch.skillbranch.R;
import com.skillbranch.skillbranch.di.components.DaggerSplashComponent;
import com.skillbranch.skillbranch.di.components.SplashComponent;
import com.skillbranch.skillbranch.di.modules.SplashModule;
import com.skillbranch.skillbranch.ui.presenters.SplashPresenter;
import com.skillbranch.skillbranch.ui.views.SplashView;

import javax.inject.Inject;

import static com.skillbranch.skillbranch.utils.UIUtils.showToast;

/**
 * @author Sergey Vorobyev.
 */

public class SplashActivity extends DaggerBaseActivity<SplashComponent> implements SplashView {

    @Inject SplashPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mPresenter.loadData();
    }

    @Override
    public void showProgress() {
        super.showProgress();
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
    }

    @Override
    public void showError(String message) {
        showToast(message);
    }

    @Override
    public void navigateToMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void setupComponent() {
        component = DaggerSplashComponent.builder()
                .appComponent(App.get().getComponent())
                .splashModule(new SplashModule(this))
                .build();
        component.inject(this);
    }
}
