package com.skillbranch.skillbranch.ui.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.data.RetainingLceViewState;
import com.skillbranch.skillbranch.R;
import com.skillbranch.skillbranch.data.db.entities.MemberEntity;
import com.skillbranch.skillbranch.di.components.MembersPageComponent;
import com.skillbranch.skillbranch.di.modules.MembersPageModule;
import com.skillbranch.skillbranch.ui.presenters.MembersPagePresenter;
import com.skillbranch.skillbranch.ui.views.MembersPageView;
import com.skillbranch.skillbranch.ui.views.activities.MainActivity;
import com.skillbranch.skillbranch.ui.views.activities.MemberDetailsActivity;
import com.skillbranch.skillbranch.ui.views.adapters.MembersPageAdapter;
import com.skillbranch.skillbranch.utils.SimpleDividerItemDecoration;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.skillbranch.skillbranch.ui.views.activities.MemberDetailsActivity.KEY_MEMBER_DETAILS_ID;

/**
 * @author Sergey Vorobyev.
 */

public class MembersPageFragment extends MvpLceViewStateDaggerBaseFragment<SwipeRefreshLayout,
        List<MemberEntity>, MembersPageView, MembersPagePresenter, MembersPageComponent>
        implements MembersPageView {

    private static final String TAG = "NewsPageFragment";
    public static final String KEY_HOUSE_ID = "KEY_HOUSE_ID";

    @Inject
    MembersPageAdapter mAdapter;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    private Unbinder unbinder;

    @Override
    protected void setupComponent() {
        Log.d(TAG, "setupComponent()");
        super.component = ((MainActivity)getActivity()).getComponent()
                .plus(new MembersPageModule(this));
        super.component.inject(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mAdapter.setOnItemClickListener(position -> presenter
                .onItemClick(mAdapter.getData().get(position).getId()));
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public LceViewState<List<MemberEntity>, MembersPageView> createViewState() {
        Log.d(TAG, "createViewState()");
        return new RetainingLceViewState<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.d(TAG, inflater.toString() + container + savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_page_members, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        Log.d(TAG, view.toString() + savedInstance);
        contentView.setOnRefreshListener(() -> loadData(true));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView()");
        unbinder.unbind();
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
        Log.d(TAG, "showLoading(" + pullToRefresh + ")");
        contentView.post(() -> contentView.setRefreshing(true));
    }

    @Override
    public void showContent() {
        super.showContent();
        Log.d(TAG, "showContent()");
        contentView.post(() -> contentView.setRefreshing(false));
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        Log.d(TAG, "showError(" + e + ", " + pullToRefresh + ")");
        contentView.post(() -> contentView.setRefreshing(false));
    }

    @Override
    protected void showLightError(String msg) {
        Log.d(TAG, "showLightError(" + msg + ")");
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        Log.d(TAG, "loadData(" + pullToRefresh + ")");
        presenter.loadNews(pullToRefresh);
    }

    @Override
    public void setData(List<MemberEntity> data) {
        Log.d(TAG, "setData(" + data + ")");
        mAdapter.setData(data);
    }

    @Override
    public List<MemberEntity> getData() {
        Log.d(TAG, "getData()");
        return mAdapter == null ? null : mAdapter.getData();
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        Log.d(TAG, "getErrorMessage(" + e + ", " + pullToRefresh + ")");
        return getString(R.string.error_unknown_error);
    }

    @NonNull
    @Override
    public MembersPagePresenter createPresenter() {
        Log.d(TAG, "createPresenter()");
        return component.presenter();
    }

    @Override
    public void navigateToMemberDetailActivity(long id) {
        Log.d(TAG, "navigateToMemberDetailActivity(" + id + ")");
        Intent i = new Intent(getContext(), MemberDetailsActivity.class);
        i.putExtra(KEY_MEMBER_DETAILS_ID, id);
        startActivity(i);
    }
}
