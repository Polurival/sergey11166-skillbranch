package com.skillbranch.skillbranch.ui.presenters.impl;

import com.skillbranch.skillbranch.ui.presenters.MembersTabsPresenter;
import com.skillbranch.skillbranch.ui.views.MembersTabsView;

/**
 * @author Sergey Vorobyev.
 */

public class MembersTabsPresenterImpl implements MembersTabsPresenter {

    private MembersTabsView mView;

    public MembersTabsPresenterImpl(MembersTabsView view) {
        mView = view;
    }
}
