package com.skillbranch.skillbranch.ui.presenters;

/**
 * @author Sergey Vorobyev
 */

public interface MainPresenter {

    void onNavigationItemSelected(int itemId);

    void onBackPressed(boolean isOpenedDrawer);
}
