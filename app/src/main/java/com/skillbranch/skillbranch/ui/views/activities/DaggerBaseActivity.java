package com.skillbranch.skillbranch.ui.views.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;

import com.skillbranch.skillbranch.R;

import static com.skillbranch.skillbranch.utils.Constants.LOG_TAG_PREFIX;
import static com.skillbranch.skillbranch.utils.UIUtils.showToast;

public abstract class DaggerBaseActivity<C> extends AppCompatActivity {

    private static final String TAG = LOG_TAG_PREFIX + "BaseActivity";
    private static final String IS_PROGRESS_SHOWING_KEY = "IS_PROGRESS_SHOWING_KEY";

    private ProgressDialog mProgressDialog;
    private boolean mIsProgressShowing;

    protected C component;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setupComponent();
        super.onCreate(savedInstanceState);
        initProgressDialog();

        if (savedInstanceState != null) {
            mIsProgressShowing = savedInstanceState.getBoolean(IS_PROGRESS_SHOWING_KEY);
        }
        if (mIsProgressShowing) showProgress();
    }

    @Override
    protected void onDestroy() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_PROGRESS_SHOWING_KEY, mIsProgressShowing);
        super.onSaveInstanceState(outState);
    }

    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(this, R.style.custom_progress);
        mProgressDialog.setCancelable(false);
        Window window = mProgressDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public void showProgress() {
        mProgressDialog.show();
        mProgressDialog.setContentView(R.layout.progress_splash);
    }

    public void hideProgress() {
        if (mProgressDialog.isShowing()) mProgressDialog.hide();
        mIsProgressShowing = false;
    }

    public void showError(String message, Throwable t) {
        Log.d(TAG, "Message: "+ message + "\n" + "Exception: " + t.toString());
        showToast(message);
    }

    public C getComponent() {
        return component;
    }

    protected abstract void setupComponent();
}
