package com.skillbranch.skillbranch.ui.views.activities;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.skillbranch.skillbranch.App;
import com.skillbranch.skillbranch.R;
import com.skillbranch.skillbranch.data.db.entities.AliasEntity;
import com.skillbranch.skillbranch.data.db.entities.MemberEntity;
import com.skillbranch.skillbranch.data.db.entities.TitleEntity;
import com.skillbranch.skillbranch.di.components.DaggerMemberDetailsComponent;
import com.skillbranch.skillbranch.di.components.MemberDetailsComponent;
import com.skillbranch.skillbranch.di.modules.MemberDetailsModule;
import com.skillbranch.skillbranch.ui.presenters.MemberDetailsPresenter;
import com.skillbranch.skillbranch.ui.views.MemberDetailsView;
import com.skillbranch.skillbranch.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.support.design.widget.Snackbar.LENGTH_LONG;

/**
 * @author Sergey Vorobyev.
 */

public class MemberDetailsActivity extends DaggerBaseActivity<MemberDetailsComponent>
        implements MemberDetailsView {

    public static final String KEY_MEMBER_DETAILS_ID = "KEY_MEMBER_DETAILS_ID";

    @BindView(R.id.coordinator_layout) CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.collapsing_toolbar_layout) CollapsingToolbarLayout mCollapsingToolbarLayout;
    @BindView(R.id.toolbar)Toolbar mToolbar;
    @BindView(R.id.photo) ImageView mPhoto;
    @BindView(R.id.words) TextView mWordsTV;
    @BindView(R.id.born) TextView mBornTV;
    @BindView(R.id.titles) TextView mTitlesTV;
    @BindView(R.id.aliases) TextView mAliasesTV;
    @BindView(R.id.father) TextView mFatherTV;
    @BindView(R.id.mother) TextView mMotherTV;

    @BindView(R.id.words_ly) View mWordsLy;
    @BindView(R.id.born_ly) View mBornLy;
    @BindView(R.id.titles_ly) View mTitlesLy;
    @BindView(R.id.aliases_ly) View mAliasesLy;
    @BindView(R.id.father_ly) View mFatherLy;
    @BindView(R.id.mother_ly) View mMotherLy;
    private Unbinder mUnbinder;

    @Inject MemberDetailsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_details);
        mUnbinder = ButterKnife.bind(this);
        setupToolbar();
        mPresenter.loadData(getIntent().getExtras().getLong(KEY_MEMBER_DETAILS_ID));
    }

    @Override
    protected void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void showData(MemberEntity data, MemberEntity father, MemberEntity mother) {
        int photo = 0;
        switch (data.getRemoteHouseId()) {
            case Constants.HOUSE_STARK_ID:
                photo = R.drawable.stark_house;
                break;
            case Constants.HOUSE_LANNISTER_ID:
                photo = R.drawable.lannister_house;
                break;
            case Constants.HOUSE_TARGARYEN_ID:
                photo = R.drawable.targaryen_house;
                break;
        }
        int screenWidth = mPhoto.getResources().getDisplayMetrics().widthPixels;
        int screenHeight = mPhoto.getResources().getDisplayMetrics().heightPixels;
        Point mPhotoSize = new Point(screenWidth, screenHeight);
        App.get().getComponent().picasso()
                .load(photo)
                .resize(mPhotoSize.x, mPhotoSize.y)
                .centerCrop()
                .into(mPhoto);

        mCollapsingToolbarLayout.setTitle(data.getName());
        if (!data.getWords().isEmpty()) {
            mWordsTV.setText(data.getWords());
        } else {
            mWordsLy.setVisibility(View.GONE);
        }
        if (!data.getBorn().isEmpty()) {
            mBornTV.setText(data.getBorn());
        } else {
            mBornLy.setVisibility(View.GONE);
        }
        List<TitleEntity> titles = data.getTitles();
        if (!titles.isEmpty()) {
            StringBuilder sbt = new StringBuilder();
            for (TitleEntity title: titles) sbt.append(title.getTitle()).append(", ");
            mTitlesTV.setText(sbt.toString());
        } else {
            mTitlesLy.setVisibility(View.GONE);
        }
        List<AliasEntity> aliases = data.getAliases();
        if (!aliases.isEmpty()) {
            StringBuilder sba = new StringBuilder();
            for (AliasEntity alias: aliases) sba.append(alias.getTitle()).append(", ");
            mAliasesTV.setText(sba.toString());
        } else {
            mAliasesLy.setVisibility(View.GONE);
        }

        if (father != null) {
            mFatherTV.setText(father.getName());
            mFatherTV.setOnClickListener(v -> mPresenter.onFatherButtonClicked());
        } else {
            mFatherLy.setVisibility(View.GONE);
        }
        if (mother != null) {
            mMotherTV.setText(mother.getName());
            mMotherTV.setOnClickListener(v -> mPresenter.onMotherButtonClicked());
        } else {
            mMotherLy.setVisibility(View.GONE);
        }
    }

    @Override
    public void navigateToAnotherMember(long id) {
        Intent i = new Intent(this, MemberDetailsActivity.class);
        i.putExtra(KEY_MEMBER_DETAILS_ID, id);
        startActivity(i);
    }

    @Override
    public void showDied(String data) {
        Snackbar.make(mCoordinatorLayout, getString(R.string.details_died) + " " + data, LENGTH_LONG).show();
    }

    @Override
    protected void setupComponent() {
        component = DaggerMemberDetailsComponent.builder()
                .appComponent(App.get().getComponent())
                .memberDetailsModule(new MemberDetailsModule(this)).build();
        component.inject(this);
    }
}
