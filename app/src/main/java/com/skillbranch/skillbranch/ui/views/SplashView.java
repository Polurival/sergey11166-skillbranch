package com.skillbranch.skillbranch.ui.views;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * @author Sergey Vorobyev
 */

public interface SplashView extends MvpView {

    void showProgress();
    void hideProgress();
    void showError(String message);
    void navigateToMainActivity();
}
