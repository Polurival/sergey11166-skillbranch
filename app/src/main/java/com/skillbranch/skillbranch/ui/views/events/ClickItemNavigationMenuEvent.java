package com.skillbranch.skillbranch.ui.views.events;

/**
 * @author Sergey Vorobyev.
 */

public class ClickItemNavigationMenuEvent {

    public ClickItemNavigationMenuEvent(String houseId) {
        this.houseId = houseId;
    }

    private String houseId;

    public String getHouseId() {
        return houseId;
    }
}
