package com.skillbranch.skillbranch.ui.views.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.ImageView;

import com.skillbranch.skillbranch.App;
import com.skillbranch.skillbranch.R;
import com.skillbranch.skillbranch.di.components.DaggerMainComponent;
import com.skillbranch.skillbranch.di.components.MainComponent;
import com.skillbranch.skillbranch.di.modules.MainModule;
import com.skillbranch.skillbranch.ui.presenters.MainPresenter;
import com.skillbranch.skillbranch.ui.views.MainView;
import com.skillbranch.skillbranch.ui.views.events.ClickItemNavigationMenuEvent;
import com.skillbranch.skillbranch.ui.views.fragments.MembersTabsFragment;
import com.skillbranch.skillbranch.utils.CircleTransformation;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.skillbranch.skillbranch.utils.Constants.HOUSE_LANNISTER_ID;
import static com.skillbranch.skillbranch.utils.Constants.HOUSE_STARK_ID;
import static com.skillbranch.skillbranch.utils.Constants.HOUSE_TARGARYEN_ID;

/**
 * @author Sergey Vorobyev.
 */

public class MainActivity extends DaggerBaseActivity<MainComponent> implements
        NavigationView.OnNavigationItemSelectedListener, MainView {

    @Inject MainPresenter presenter;

    @BindView(R.id.naw_view) NavigationView mNavigationView;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;

    @Override
    protected void setupComponent() {
        component = DaggerMainComponent.builder()
                .appComponent(App.get().getComponent())
                .mainModule(new MainModule(this))
                .build();
        component.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupNavigationView();
    }

    private void setupNavigationView() {
        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setCheckedItem(R.id.nav_item_1);
        onNavigationItemSelected(mNavigationView.getMenu().getItem(0));
        ImageView avatar = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.avatar);

        App.get().getComponent().picasso().load(R.mipmap.ic_lanister)
                .resizeDimen(R.dimen.size_avatar, R.dimen.size_avatar)
                .onlyScaleDown()
                .centerCrop()
                .transform(new CircleTransformation())
                .into(avatar);
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed(mDrawerLayout.isDrawerOpen(GravityCompat.START));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        presenter.onNavigationItemSelected(id);
        return true;
    }

    @Override
    public void showFragmentInTabs1() {
        setFragment(HOUSE_STARK_ID);
    }

    @Override
    public void showFragmentInTabs2() {
        setFragment(HOUSE_LANNISTER_ID);
    }

    @Override
    public void showFragmentInTabs3() {
        setFragment(HOUSE_TARGARYEN_ID);
    }

    void setFragment(String houseId) {
        MembersTabsFragment housesTabsFragment;
        FragmentManager fm = getSupportFragmentManager();
        housesTabsFragment = (MembersTabsFragment) fm.findFragmentByTag(MembersTabsFragment.FRAGMENT_TAG);
        if (housesTabsFragment == null) housesTabsFragment = new MembersTabsFragment();
        fm.beginTransaction()
                .replace(R.id.main_container, housesTabsFragment, MembersTabsFragment.FRAGMENT_TAG)
                .commit();
        EventBus.getDefault().post(new ClickItemNavigationMenuEvent(houseId));
    }

    @Override
    public void closeDrawer() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void pressBack() {
        super.onBackPressed();
    }
}