package com.skillbranch.skillbranch.ui.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skillbranch.skillbranch.R;
import com.skillbranch.skillbranch.data.db.entities.MemberEntity;
import com.skillbranch.skillbranch.utils.CircleTransformation;
import com.skillbranch.skillbranch.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Sergey Vorobyev.
 */

public class MembersPageAdapter extends RecyclerView.Adapter<MembersPageAdapter.MemberViewHolder> {

    private Picasso mPicasso;
    private Context mContext;
    private List<MemberEntity> mData;
    private OnItemClickListener mOnItemClickListener;

    public MembersPageAdapter(Picasso picasso) {
        this.mPicasso = picasso;
        mData = new ArrayList<>();
    }

    @Override
    public MemberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_members_list, parent, false);
        return new MemberViewHolder(view, mOnItemClickListener);
    }

    @Override
    public void onBindViewHolder(MemberViewHolder holder, int position) {
        MemberEntity memberEntity = mData.get(position);

        holder.mMemberName.setText(memberEntity.getName());

        int avatar = 0;
        switch (memberEntity.getRemoteHouseId()) {
            case Constants.HOUSE_STARK_ID:
                avatar = R.mipmap.ic_stark;
                break;
            case Constants.HOUSE_LANNISTER_ID:
                avatar = R.mipmap.ic_lanister;
                break;
            case Constants.HOUSE_TARGARYEN_ID:
                avatar = R.mipmap.ic_targaryen;
                break;
        }

        mPicasso.load(avatar)
                .resizeDimen(R.dimen.size_avatar, R.dimen.size_avatar)
                .onlyScaleDown()
                .centerCrop()
                .transform(new CircleTransformation())
                .into(holder.mAvatar);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    public List<MemberEntity> getData() {
        return mData;
    }

    public void setData(@NonNull List<MemberEntity> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    static class MemberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.avatar) ImageView mAvatar;
        @BindView(R.id.text) TextView mMemberName;

        private OnItemClickListener mOnItemClickListener;

        MemberViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mOnItemClickListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) mOnItemClickListener.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
